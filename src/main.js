import Vue from "vue";
import App from "./App.vue";
import VueGtag from "vue-gtag";

// Vuesax Component Framework
import Vuesax from "vuesax";
import "material-icons/iconfont/material-icons.css"; //Material Icons
import "vuesax/dist/vuesax.css"; // Vuesax
import VueScrollTo from "vue-scrollto";
import infiniteScroll from "vue-infinite-scroll";
import VueAwesomeSwiper from "vue-awesome-swiper";
import "swiper/swiper-bundle.css";
import VueAnimateOnScroll from "vue-animate-onscroll";
import Skeleton from "vue-loading-skeleton";
import VueObserveVisibility from "vue-observe-visibility";
import VueLazyload from "vue-lazyload";
import "./filters/filters.js";
import "../themeConfig.js";
import "./globalComponents.js";
import "./assets/scss/main.scss";
import "@/assets/css/main.css";
import router from "./router";
import store from "./store/store";

Vue.use(Vuesax);
Vue.use(Skeleton);
Vue.use(VueScrollTo);
Vue.use(infiniteScroll);
Vue.use(VueAwesomeSwiper);
Vue.use(VueAnimateOnScroll);
Vue.use(VueObserveVisibility);
Vue.use(VueLazyload);
Vue.use(
  VueGtag,
  {
    config: {id: "G-40YZM9RRT6"}
  },
  router
);

// axios
import axios from "./axios.js";
Vue.prototype.$http = axios;
const token = localStorage.getItem("token");
if (token) {
  Vue.prototype.$http.defaults.headers.common["Authorization"] = "Bearer " + token;
}

// Vuejs - Vue wrapper for hammerjs
import {VueHammer} from "vue2-hammer";
Vue.use(VueHammer);

import TextareaAutosize from "vue-textarea-autosize";
Vue.use(TextareaAutosize);

// import Vuetify, { VTextarea } from "vuetify";

// Vue.use(Vuetify, {
//   components: {
//     VTextarea
//   }
// });

// PrismJS
import "prismjs";
import "prismjs/themes/prism-tomorrow.css";

// Feather font icon
require("./assets/css/iconfont.css");

// Vue select css
// Note: In latest version you have to add it separately
// import 'vue-select/dist/vue-select.css';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
