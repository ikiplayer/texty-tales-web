import Vue from "vue";
import Router from "vue-router";
import store from "./store/store.js";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  // scrollBehavior() {
  //   return {x: 0, y: 0};
  // },
  routes: [
    {
      // =============================================================================
      // MAIN LAYOUT ROUTES
      // =============================================================================
      path: "",
      component: () => import("./layouts/main/Main.vue"),
      children: [
        // =============================================================================
        // Theme Routes
        // =============================================================================
        {
          path: "/",
          name: "home",
          component: () => import("./views/pages/home/Index.vue")
        },
        // {
        //   path: "/home",
        //   name: "home",
        //   component: () => import("./views/pages/home/Index.vue")
        // },
        {
          path: "/top",
          name: "top",
          component: () => import("./views/pages/category/Category.vue"),
          meta: {
            emojiIcon: "💯",
            title: "Top"
          }
        },
        {
          path: "/comedy",
          name: "comedy",
          component: () => import("./views/pages/category/Category.vue"),
          meta: {
            emojiIcon: " 😂",
            title: "Comedy"
          }
        },
        {
          path: "/drama",
          name: "drama",
          component: () => import("./views/pages/category/Category.vue"),
          meta: {
            emojiIcon: "😮",
            title: "Drama"
          }
        },
        {
          path: "/thriller",
          name: "thriller",
          component: () => import("./views/pages/category/Category.vue"),
          meta: {
            emojiIcon: "😱",
            title: "Thriller"
          }
        },
        {
          path: "/romance",
          name: "romance",
          component: () => import("./views/pages/category/Category.vue"),
          meta: {
            emojiIcon: "😘",
            title: "Romance"
          }
        },
        {
          path: "/more",
          name: "more",
          component: () => import("./views/pages/category/Category.vue"),
          meta: {
            emojiIcon: "💬",
            title: "More"
          }
        },
        {
          path: "/chat/create-selection",
          name: "chat.create-selection",
          component: () => import("./layouts/components/create-chat-selection/CreateChatSelection.vue"),
          meta: {
            no_scroll: true
          }
        },
        {
          path: "/chat/create/1",
          name: "chat.create.type1",
          component: () => import("./layouts/components/create-chat/ChatType1.vue"),
          meta: {
            no_scroll: true,
            has_footer: false
          }
        },
        {
          path: "/chat/create/1/:chatId",
          name: "chat.create.type1.update",
          component: () => import("./layouts/components/create-chat/ChatType1.vue"),
          meta: {
            no_scroll: true,
            has_footer: false
          }
        },
        {
          path: "/chat/create/2",
          name: "chat.create.type2",
          component: () => import("./layouts/components/create-chat/ChatType2.vue"),
          meta: {
            no_scroll: true,
            has_footer: false
          }
        },
        {
          path: "/chat/create/2/:chatId",
          name: "chat.create.type2.update",
          component: () => import("./layouts/components/create-chat/ChatType2.vue"),
          meta: {
            no_scroll: true,
            has_footer: false
          }
        },
        {
          path: "/chat/:chatId",
          name: "chat.view",
          component: () => import("./layouts/components/chat/Chat.vue"),
          meta: {
            no_scroll: true,
            has_footer: false
          }
        },
        {
          path: "/profile",
          name: "profile",
          component: () => import("@/views/pages/profile/ProfileIndex.vue")
        },
        {
          path: "/profile/favorites",
          name: "profile.favorites",
          component: () => import("./views/pages/category/Category.vue"),
          meta: {
            emojiIcon: "❤️‍",
            title: "My Favorites",
            hasFiltering: false,
            viewType: "my_likes"
          }
        },
        {
          path: "/profile/chats",
          name: "profile.chats",
          component: () => import("./views/pages/category/Category.vue"),
          meta: {
            emojiIcon: "💬‍",
            title: "My Chats",
            hasFiltering: false,
            viewType: "my_chats"
          }
        },
        {
          path: "/login",
          name: "login",
          component: () => import("@/views/pages/Login2.vue")
        },
        {
          path: "/register",
          name: "register",
          component: () => import("@/views/pages/Register.vue")
        },
        {
          path: "/forgot-password",
          name: "forgot-pasword",
          component: () => import("@/views/pages/ForgotPassword.vue")
        },
        {
          path: "/reset-account/:otpCode",
          name: "reset-account",
          component: () => import("@/views/pages/ResetAccount.vue")
        }
      ]
    },
    // =============================================================================
    // FULL PAGE LAYOUTS
    // =============================================================================
    {
      path: "",
      component: () => import("@/layouts/full-page/FullPage.vue"),
      children: [
        {
          path: "/pages/login",
          name: "page-login",
          component: () => import("@/views/pages/Login.vue")
        },
        {
          path: "/pages/error-404",
          name: "page-error-404",
          component: () => import("@/views/pages/Error404.vue")
        },
        {
          path: "/confirm-account/:otpCode",
          name: "confirm-account",
          component: () => import("@/views/pages/ConfirmAccount.vue")
        }
      ]
    },
    {
      path: "/puppeteer/chat/2/:chatId",
      component: () => import("./layouts/components/puppeteer/Chat2/Chat.vue")
    },
    {
      path: "/puppeteer/chat/1/:chatId",
      component: () => import("./layouts/components/puppeteer/Chat1/Chat.vue")
    },
    // Redirect to 404 page, if no match found
    {
      path: "*",
      redirect: "/pages/error-404"
    }
  ]
});

router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById("loading-bg");
  if (appLoading) {
    appLoading.style.display = "none";
  }
});

router.beforeEach((to, from, next) => {
  if (to.name == "chat.create-selection" && !store.state.AppActiveUser.token_id) {
    next("/");
  } else if (to.name == "chat.create.type1" && !store.state.AppActiveUser.token_id) {
    next("/");
  } else if (to.name == "chat.create.type2" && !store.state.AppActiveUser.token_id) {
    next("/");
  } else if (to.name == "profile" && !store.state.AppActiveUser.token_id) {
    next("/");
  } else if (to.name == "profile.favorites" && !store.state.AppActiveUser.token_id) {
    next("/");
  } else if (to.name == "profile.chats" && !store.state.AppActiveUser.token_id) {
    next("/");
  } else {
    next();
  }
});

export default router;
