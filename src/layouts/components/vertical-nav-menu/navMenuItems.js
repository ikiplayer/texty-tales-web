export default [
  {
    url: "/",
    name: "Home",
    slug: "home",
    icon: "HomeIcon",
    emojiIcon: "🏠"
  },
  {
    url: "/top",
    name: "Top",
    slug: "top",
    icon: "FileIcon",
    emojiIcon: "💯",
    params: {
      emojiIcon: "💯",
      title: "Top"
    }
  },
  {
    url: "/comedy",
    name: "Comedy",
    slug: "comedy",
    icon: "FileIcon",
    emojiIcon: "😂",
    params: {
      emojiIcon: " 😂",
      title: "Comedy"
    }
  },
  {
    url: "/drama",
    name: "Drama",
    slug: "drama",
    icon: "FileIcon",
    emojiIcon: "😮",
    params: {
      emojiIcon: "😮",
      title: "Drama"
    }
  },
  {
    url: "/thriller",
    name: "Thriller",
    slug: "thriller",
    icon: "FileIcon",
    emojiIcon: "😱",
    params: {
      emojiIcon: "😱",
      title: "Thriller"
    }
  },
  {
    url: "/romance",
    name: "Romance",
    slug: "romance",
    icon: "FileIcon",
    emojiIcon: "😘",
    params: {
      emojiIcon: "😘",
      title: "Romance"
    }
  },
  {
    url: "/more",
    name: "More",
    slug: "more",
    icon: "FileIcon",
    emojiIcon: "💬",
    params: {
      emojiIcon: "💬",
      title: "More"
    }
  }
];
